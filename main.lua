-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------
_W = display.contentWidth
_H = display.contentHeight
_T = display.screenOriginY -- Top
_L = display.screenOriginX -- Left
_R = display.viewableContentWidth - _L -- Right
_B = display.viewableContentHeight - _T-- Bottom
_CX = math.floor(_W / 2) -- centr x
_CY = math.floor(_H / 2) -- centr y

app = {}
app.name='ScrollView Viewer' 
app.version = '1.0.0'
app.isAndroid = false
app.isSimulator = true
app.font = native.systemFont
app.fontbold = native.systemFontBold
app.duration = 200
function app.showMsg(txt)
    if type(txt) == 'string' then
        native.showAlert(app.name, txt, {'OK'}, function() end)
    end
end
if system.getInfo('environment') ~= 'simulator' then
    io.output():setvbuf('no')
else
    app.isSimulator = true
end
local platform = system.getInfo('platformName')
if platform == 'Android' then
    app.isAndroid = true
end

screenBoard = require( "composer" )
widget = require('widget')
json = require( "json" ) 


-- Reserve channel 1 for background music
audio.reserveChannels( 1 )
-- Reduce the overall volume of the channel
audio.setVolume( 0.2, { channel=1 } )

local filePath = system.pathForFile( "images.json", system.ResourceDirectory  )
local function loadFile(strFilePath)
 
    local file = io.open( strFilePath, "r" )
 
    if file then
        local contents = file:read( "*a" )
        io.close( file )
        return contents
    else
    	return nil    
    end    
end 
 
 

screenTables=json.decode(loadFile(filePath))
filelist = {}
for pos,val in pairs(screenTables.images) do
	table.insert(filelist,{URLfile=val,filename=val:sub(val:len()-string.find(val:reverse(),"/") +2 ),exists = false})
end


local function main()
    math.randomseed(os.time())
	display.setDefault('background', 0.6, 0.6, 1)
    screenBoard.gotoScene('scenes.menu', {effect = 'slideLeft', time = app.duration})
end
main()

