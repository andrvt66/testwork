local scene = screenBoard.newScene()
local function networkListener( event )
       if ( event.isError ) then
        print( "Network error - download failed: ", event.response )
    elseif ( event.phase == "began" ) then
        print( "Progress Phase: began" )
    elseif ( event.phase == "ended" ) then
        print( "Displaying response image file" )
        
    end

end


-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------




-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )
  local sceneGroup = self.view
local filesMissing = false
for i=1, #filelist do
        local localfilePath = system.pathForFile( filelist[i]["filename"], system.CachesDirectory )      
    if ( localfilePath ) then
        local localfile, errorString = io.open( localfilePath, "r" )
        if not localfile then
            filesMissing = true
        -- Initiate file download
            network.download( filelist[i]['URLfile'], "GET", networkListener, filelist[i]["filename"], system.CachesDirectory)
        else
	       filelist[i]["exists"] = true
            localfile:close()
        end
         localfilePath = nil
         localfile = nil
    end
  
end

    local appname = display.newText(app.name, _CX, _CY-200, app.fontbold, 35 )
    local appname2 = display.newText("(Тестовое задание)", _CX, _CY-150, app.fontbold, 30 )
	-- Code here runs when the scene is first created but has not yet appeared on screen
local pleaseWait = display.newText('Пожалуйста ожидайте...',_CX, _CY, app.font, 16 )
pleaseWait:setFillColor(0,0,0)
pleaseWait.isVisible = true

local button = widget.newButton(
{
        width = 200,
        height = 60,
        defaultFile = 'images/button.png',
        overFile = 'images/button-over.png',
        labelColor = {default = {0, 0, 0}, over = {1, 1, 1}},
        fontSize = 18,
        label = 'Запустить',
        onRelease = function()
            screenBoard.gotoScene('scenes.choose', {effect = 'slideLeft', time = app.duration})
        end
}        
        )
button.x = _CX
button.y = _CY
button.emboss = true
button.isVisible = false

local button_q = widget.newButton(
{
        width = 200,
        height = 60,
        defaultFile = 'images/button.png',
        overFile = 'images/button-over.png',
        labelColor = {default = {0, 0, 0}, over = {1, 1, 1}},
        fontSize = 18,
        label = 'Выйти',
        onRelease = function()
            native.requestExit()
        end
}        
        )
button_q.x = _CX
button_q.y = _CY+160
button_q.emboss = true
button_q.isVisible = false

local button_r = widget.newButton(
{
        width = 200,
        height = 60,
        defaultFile = 'images/button.png',
        overFile = 'images/button-over.png',
        labelColor = {default = {0, 0, 0}, over = {1, 1, 1}},
        fontSize = 18,
        label = 'Описание',
        onRelease = function()
            screenBoard.gotoScene('scenes.readme', {effect = 'slideLeft', time = app.duration})
        end
}        
        )
button_r.x = _CX
button_r.y = _CY+80
button_r.emboss = true
button_r.isVisible = true

local url = "http://www.google.com"
 
if ( system.canOpenURL( url ) ) then
    button.isVisible = true
    button_q.isVisible = true
    pleaseWait.isVisible = false
else
   print(2)
    native.showAlert( "Ошибка подключения к Интернету!","Ваше устройство не подключено к Интернету!" .. '\n' .. 'Запуск не возможен!' )
    
    button_q.isVisible = true
    pleaseWait.isVisible = false
end

sceneGroup:insert(button)
sceneGroup:insert(button_q)
sceneGroup:insert(button_r)
sceneGroup:insert(pleaseWait)
sceneGroup:insert(appname)
sceneGroup:insert(appname2)
end



-- hide()
function scene:hide( event )

	local sceneGroup = self.view
    local previous_scene = screenBoard.getSceneName("previous")
    if previous_scene then
        screenBoard.removeScene(previous_scene)
    end

	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end





-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "hide", scene )

-- -----------------------------------------------------------------------------------

return scene
