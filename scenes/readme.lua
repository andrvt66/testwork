local scene = screenBoard.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------




-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen
-- Read from the ReadMe.txt file
local backButton = widget.newButton(
	{
        width = 100,
        height = 40,
        defaultFile = 'images/button.png',
        overFile = 'images/button-over.png',
        labelColor = {default = {0, 0, 0}, over = {1, 1, 1}},
        font = app.fontbold ,
		fontSize = 16,
        label = 'Меню',
        onRelease = function()
            screenBoard.gotoScene('scenes.menu', {effect = 'slideRight', time = app.duration})
        end}
        )
backButton.x = _CX
backButton.y = _H
backButton.emboss = true
local readMeText = ""
local readMeFilePath = system.pathForFile( "ReadMe.txt", system.ResourceDirectory )
if readMeFilePath then
	local readMeFile = io.open( readMeFilePath )
	local rt = readMeFile:read( "*a" )
		local function trimString( s )
			return string.match( s, "^()%s*$" ) and "" or string.match( s, "^%s*(.*%S)" )
		end
	rt = trimString( rt )
		if string.len( rt ) > 0 then
		 readMeText = rt 
		end
	io.close( readMeFile ) ; rt = nil
end

local scrollTxt = widget.newScrollView
		{
			x = _CX,
			y = _CY-100,
        	width = _W-10,
        	height = 400,
			topPadding = 0,
			bottomPadding = 0
		}

local infoText = display.newText("",  10, 10, 300, 0, app.font, 12 )
infoText:setFillColor( 0 )
infoText.anchorY = 0
infoText.anchorX = -30
infoText.text = "\n"..readMeText.."\n\n"
scrollTxt:insert(infoText)
sceneGroup:insert(scrollTxt)
sceneGroup:insert(backButton)
end
-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase
       local previous_scene = screenBoard.getSceneName("previous")
    if previous_scene then
        screenBoard.removeScene(previous_scene)
    end


	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
