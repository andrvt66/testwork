

local scene = screenBoard.newScene()
local indFoto = 1
local curFoto,scrollView,nextButton,prevButton
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
local function eventBtn(event)

	if event.target.id == 'nextBtn' then
		indFoto = indFoto + 1
		
		if indFoto <= #filelist then
			scrollView:remove(curFoto)
			if indFoto>1 and indFoto <= #filelist then
			  prevButton.isVisible = true 
			end
			curFoto = display.newImage(filelist[indFoto]["filename"], system.CachesDirectory, 200, 400)
    		scrollView:insert(curFoto)
		else
			indFoto = #filelist
		
			nextButton.isVisible = false 
			app.showMsg('Достигнут конец просмотра!')
		end
	elseif event.target.id == 'prevBtn' then
		indFoto = indFoto - 1
		if indFoto>=1 and indFoto <= #filelist then
			  nextButton.isVisible = true 
		end
		if indFoto >= 1  and indFoto <= #filelist then
			scrollView:remove(curFoto)
			curFoto = display.newImage(filelist[indFoto]["filename"], system.CachesDirectory, 200, 400)
    		scrollView:insert(curFoto)
		else
			indFoto = 1
			
			prevButton.isVisible = false 
			--nextButton.isVisible = true 
			app.showMsg('Достигнуто начало просмотра!')
		end
	end
end


local function scrollListener( event )
 
    local phase = event.phase
    if ( phase == "began" ) then print( "Scroll view was touched" )
    elseif ( phase == "moved" ) then print( "Scroll view was moved" )
    elseif ( phase == "ended" ) then print( "Scroll view was released" )
    end
 
    -- In the event a scroll limit is reached...
    if ( event.limitReached ) then
        if ( event.direction == "up" ) then print( "Reached bottom limit" )
        elseif ( event.direction == "down" ) then print( "Reached top limit" )
        elseif ( event.direction == "left" ) then print( "Reached right limit" )
        elseif ( event.direction == "right" ) then print( "Reached left limit" )
        end
    end
 
    return true
end




-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

	local sceneGroup = self.view
	local backButton = widget.newButton(
	{
        width = 100,
        height = 40,
        defaultFile = 'images/button.png',
        overFile = 'images/button-over.png',
        labelColor = {default = {0, 0, 0}, over = {1, 1, 1}},
        font = app.fontbold ,
		fontSize = 16,
        label = 'Меню',
        onRelease = function()
            screenBoard.gotoScene('scenes.menu', {effect = 'slideRight', time = app.duration})
        end}
        )
	nextButton = widget.newButton(
	{
        id = 'nextBtn',
        width = 100,
        height = 40,
        defaultFile = 'images/button.png',
        overFile = 'images/button-over.png',
        labelColor = {default = {0, 0, 0}, over = {1, 1, 1}},
        font = app.fontbold ,
        fontSize = 16,
        label = '>>',
        onRelease = eventBtn}
        )
	prevButton = widget.newButton(
	{
        id = 'prevBtn',
        width = 100,
        height = 40,
        defaultFile = 'images/button.png',
        overFile = 'images/button-over.png',
        labelColor = {default = {0, 0, 0}, over = {1, 1, 1}},
        font = app.fontbold ,
        fontSize = 16,
        label = '<<',
        onRelease = eventBtn}
        )
	backButton.x = _CX
	backButton.y = _H
	nextButton.x = _CX+100
	nextButton.y = _H
	prevButton.x = _CX-100
	prevButton.y = _H
	scrollView = widget.newScrollView(
    {
        x = _CX,
        y = _CY-100,
        width = _W-10,
        height = 500,
        horizontalScrollDisabled = false,
        verticalScrollDisabled  = false,
        scrollWidth = 600,
        scrollHeight = 800,
        hideScrollBar = false,
        hideBackground = true,
        listener = scrollListener
    }
	)
	--scrollView:setIsLocked( true )
	sceneGroup:insert(scrollView)
	sceneGroup:insert(backButton)
	sceneGroup:insert(nextButton)
	sceneGroup:insert(prevButton)

	curFoto = display.newImage(filelist[indFoto]["filename"], system.CachesDirectory, 200, 400)
    scrollView:insert(curFoto)
    prevButton.isVisible = false 
  
end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
local previous_scene = screenBoard.getSceneName("previous")
    if previous_scene then
        screenBoard.removeScene(previous_scene)
    end
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "hide", scene )
-- -----------------------------------------------------------------------------------

return scene
